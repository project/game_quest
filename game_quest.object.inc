<?php

/**
 * @file
 * Defines the game_quest object.
 *
 * Note that this is not the same as the game quest node, but is simply an
 * association mapping between quests and characters.
 */

class game_quest {
  // The game quest nid.
  public $qid;

  // The game character nid or uid.
  public $cid;

  // Is the quest visible to the character?
  public $exposed = FALSE;
  public $exposed_timestamp = 0;

  // Is the quest is available for acceptance or completion by the character?
  public $available = FALSE;
  public $available_timestamp = 0;

  // Has the quest been accepted by the character?
  public $accepted = FALSE;
  public $accepted_timestamp = 0;

  // Has the quest been successfully completed by the character?
  public $completed = FALSE;
  public $completed_timestamp = 0;

  // Is this quest object association to be inserted?
  public $is_new = FALSE;

  // Set when a property in the quest object has been changed.
  // The object will be automatically saved on destruct in such a case.
  public $changed = FALSE;

  // Set when the game quest object has been deleted.
  // Ensures it won't be inadvertently rewritten to the database.
  public $deleted = FALSE;

  function __construct($qid, $cid) {
    $this->qid = $qid;
    $this->cid = $cid;

    // Attempt to read the quest object from the DB.
    if (!$this->read()) {
      $this->is_new = $this->changed = TRUE;
    }
  }

  function __destruct() {
    if (($this->is_new || $this->changed) && !$this->deleted) {
      $this->write();
    }
  }

  function set_exposed($exposed = TRUE, $timestamp = NULL) {
    $this->exposed_timestamp = $this->_timestamp($timestamp);
    $this->changed = TRUE;
    return $this->exposed = $exposed;
  }

  function set_available($available = TRUE, $timestamp = NULL) {
    $this->available_timestamp = $this->_timestamp($timestamp);
    $this->changed = TRUE;
    return $this->available = $available;
  }

  function set_accepted($accepted = TRUE, $timestamp = NULL) {
    $this->accepted_timestamp = $this->_timestamp($timestamp);
    $this->changed = TRUE;
    return $this->accepted = $accepted;
  }

  function set_completed($completed = TRUE, $timestamp = NULL) {
    $this->completed_timestamp = $this->_timestamp($timestamp);
    $this->changed = TRUE;
    return $this->completed = $completed;
  }

  /**
   * Write this quest object association to the database.
   */
  function write() {
    $this->deleted = FALSE;
    if ($this->is_new) {
      $this->is_new = $this->changed = FALSE;
      return drupal_write_record('game_quests', $this);
    }
    else {
      $this->changed = FALSE;
      return drupal_write_record('game_quests', $this, array('qid', 'cid'));
    }
  }

  function read() {
    if ($this->qid && $this->cid) {
      if ($quest = db_fetch_object(db_query("SELECT
          qid,
          cid,
          exposed,
          available,
          accepted,
          completed,
          exposed_timestamp,
          available_timestamp,
          accepted_timestamp,
          completed_timestamp
          FROM {game_quests} WHERE qid = %d AND cid = %d", $this->qid, $this->cid))) {
        $this->qid = $quest->qid;
        $this->cid = $quest->cid;
        $this->exposed = $quest->exposed;
        $this->available = $quest->available;
        $this->accepted = $quest->accepted;
        $this->completed = $quest->completed;
        $this->exposed_timestamp = $quest->exposed_timestamp;
        $this->available_timestamp = $quest->available_timestamp;
        $this->accepted_timestamp = $quest->accepted_timestamp;
        $this->completed_timestamp = $quest->completed_timestamp;
        return TRUE;
      }
    }
    return FALSE;
  }

  function delete() {
    $this->deleted = TRUE;
    db_query("DELETE FROM {game_quests} WHERE qid = %d AND cid = %d", $this->qid, $this->cid);
  }

  private function _timestamp($timestamp = NULL) {
    if (!isset($timestamp)) {
      $timestamp = time();
    }
    return $timestamp;
  }
}
