<?php

/**
 * @file Contains the variables and defaults used by Game Utilities: Quest.
 */

/**
 *  Wrapper for variable_get() that uses the Game Quest variable registry.
 *
 *  @param string $name
 *    The variable name to retrieve. Note that it will be namespaced by
 *    pre-pending 'game_quest__', as to avoid variable collisions with other modules.
 *  @param unknown $default
 *    An optional default variable to return if the variable hasn't been set
 *    yet. Note that within this module, all variables should already be set
 *    in the game_quest_variable_default() function.
 *  @return unknown
 *    Returns the stored variable or its default.
 *
 *  @see game_quest_variable_set()
 *  @see game_quest_variable_del()
 *  @see game_quest_variable_default()
 */
function game_quest_variable_get($name, $default = NULL) {
  // Allow for an override of the default.
  // Useful when a variable is required (like $path), but namespacing still desired.
  if (!isset($default)) {
    $default = game_quest_variable_default($name);
  }
  // Namespace all variables
  $variable_name = 'game_quest__' . $name;
  return variable_get($variable_name, $default);
}

/**
 *  Wrapper for variable_set() that uses the Game Quest variable registry.
 *
 *  @param string $name
 *    The variable name to set. Note that it will be namespaced by
 *    pre-pending 'game_quest__', as to avoid variable collisions with other modules.
 *  @param unknown $value
 *    The value for which to set the variable.
 *  @return unknown
 *    Returns the stored variable after setting.
 *
 *  @see game_quest_variable_get()
 *  @see game_quest_variable_del()
 *  @see game_quest_variable_default()
 */
function game_quest_variable_set($name, $value) {
  $variable_name = 'game_quest__' . $name;
  return variable_set($variable_name, $value);
}

/**
 *  Wrapper for variable_del() that uses the Game Quest variable registry.
 *
 *  @param string $name
 *    The variable name to delete. Note that it will be namespaced by
 *    pre-pending 'game_quest__', as to avoid variable collisions with other modules.
 *
 *  @see game_quest_variable_get()
 *  @see game_quest_variable_set()
 *  @see game_quest_variable_default()
 */
function game_quest_variable_del($name) {
  $variable_name = 'game_quest__' . $name;
  variable_del($variable_name);
}

/**
 *  The default variables within the Game Quest namespace.
 *
 *  @param string $name
 *    Optional variable name to retrieve the default. Note that it has not yet
 *    been pre-pended with the 'game_quest__' namespace at this time.
 *  @return unknown
 *    The default value of this variable, if it's been set, or NULL, unless
 *    $name is NULL, in which case we return an array of all default values.
 *
 *  @see game_quest_variable_get()
 *  @see game_quest_variable_set()
 *  @see game_quest_variable_del()
 */
function game_quest_variable_default($name = NULL) {
  static $defaults;

  if (!isset($defaults)) {
    $defaults = array(
      // The node types associated with game quests.
      'node_types' => array(),
    );
  }

  if (!isset($name)) {
    return $defaults;
  }

  if (isset($defaults[$name])) {
    return $defaults[$name];
  }
}

function game_quest_variable_name($name) {
  return 'game_quest__' . $name;
}
