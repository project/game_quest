<?php

/**
 * @file
 * Admin functions for the Game Quest module.
 */

/**
 * Configure Game Quest; menu callback for admin/settings/game_quest.
 */
function game_quest_settings() {
  $form = array();

  $types = node_get_types('names');

  $form['nodes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Game quest types'),
    '#collapsible' => TRUE,
  );
  $form['nodes'][game_quest_variable_name('node_types')] = array(
    '#type' => 'checkboxes',
    '#title' => t('Game quest types'),
    '#default_value' => game_quest_variable_get('node_types'),
    '#options' => $types,
    '#description' => t("Select the node type(s) that will be used for quests."),
  );

  return system_settings_form($form);
}
