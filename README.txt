
CONTENTS OF THIS FILE
---------------------

 * Introduction and Initial Design Goals
 * Installation
 * Developer's API
 * TODO

INTRODUCTION AND INITIAL DESIGN GOALS
=====================================

The Game Utilities: Quest module provides a structure and API for Game Quest
nodes on your site. You can set one or more node types to be Game Quest types
from the Game quests administration page, at admin/settings/game_quest. At that
point, any nodes of that type will be set as game quests.

By itself, this API currently does little more. However, it may be used in
conjunction with other modules. See the project page for other modules
currently using the Game Quest API.


INSTALLATION
============

1. Copy the files to your sites/SITENAME/modules directory.
   Or, alternatively, to your sites/all/modules directory.

2. Enable the Game Utilities: Quest module at admin/build/modules.

3. You can configure Game Quests at admin/settings/game_quest.
   Make sure to enable at least one game quest node type from this page.

4. Set any required permissions at admin/user/permissions.

5. To create a quest, head to node/add/[node-type].

DEVELOPER'S API
===============

Game quests are associated to characters by using objects of the game_quest
class. This class takes care of automatically reading and writing from the
database using the constructor and destructor. Note that this module will
automatically interface with the Game Utilities: Character module, so that if
you associated nodes with characters (rather than users), the currently
active character node nid will be used automatically in this API as a default
for $cid. Otherwise, the current $user->uid will be used.

The following functions are available through the API:

game_quest_expose($qid, $cid = NULL, $exposed = TRUE, $timestamp = NULL);
game_quest_make_available($qid, $cid = NULL, $available = TRUE, $timestamp = NULL);
game_quest_accept($qid, $cid = NULL, $accepted = TRUE, $timestamp = NULL);
game_quest_complete($qid, $cid = NULL, $completed = TRUE, $timestamp = NULL);

Additionally, a fully populated game quest association object may be loaded with

game_quest_load($qid, $cid = NULL);

Note that this will create a new game quest object associated with the character
if it doesn't currently exist in the database.

If you really need to work on the object itself, it's best to use the internal
functions provided by the class, as this will ensure that changes to objects
will be automatically written to the database during the system shutdown. You
can find the class definition in game_quest.object.inc.

TODO
====
* Implement hook_node_grants() and hook_node_access_records(), so that quests
  not exposed to the character will not normally be accessible.
* Integrate with Views, so that we can easily create lists of exposed and
  completed quests.
